from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required

# Create your views here.
from django.http.response import JsonResponse
from blue.models import Employee
from .forms import EmployeeForm

@login_required(login_url='/accounts/login/')
def create_employee(request):
    context = {}

    form = EmployeeForm(request.POST or None)
    if form.is_valid():
        form.save()
    
    context['form']= form
    
    return render(request, "blue/create.html", context)

@login_required(login_url='/accounts/login/')
def get_employees(request):
    start = request.GET.get('date_min', None)
    end = request.GET.get('date_max', None)

    if not start and not end:
        data = Employee.objects.all()
    else:
        data = Employee.objects.filter(date_of_joining__gte=start, date_of_joining__lte=end)

    print('----->',data, start, end)
    context = {'users_list': data}
    return render(request, 'blue/index.html', context)

@login_required(login_url='/accounts/login/')
def employee_details(request, employee_id):
    data = get_object_or_404(Employee, pk=employee_id)
    context = {'users_list': data}
    return render(request, 'blue/details.html', context)


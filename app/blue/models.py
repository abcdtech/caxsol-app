from django.db import  models

class Blue(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Department(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Employee(models.Model):
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    date_of_joining = models.DateTimeField(auto_now=True)
    photo = models.FileField(upload_to ='uploads/')

    def __str__(self):
        return f'{self.first_name} {self.last_name} : {self.department}'


